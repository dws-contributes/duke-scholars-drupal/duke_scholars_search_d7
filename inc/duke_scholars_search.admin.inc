<?php

function duke_scholars_search_admin_form($form, &$form_state) {

  $form['duke_scholars_search_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Scholars Search Widget Title'),
    '#description' => t('The title that appears above the Search Results'),
    '#default_value' => variable_get('duke_scholars_search_title', 'Find a Collaborator')
  );

  $form['duke_scholars_search_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Scholars Search API key'),
    '#description' => t('The API key for the Duke Scholars Search service'),
    '#default_value' => variable_get('duke_scholars_search_api_key', '95ac684a14f1468f94bf635feb00d7c6'),
  );

  return system_settings_form($form);
}
