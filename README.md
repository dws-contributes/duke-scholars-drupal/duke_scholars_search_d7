# DukeScholars Search

This is a small drupal module that wraps an equally small React search module.

This module provides a block that is used to house both the search form as well as the results. 

## To build the javascript assets:
- run `make clone`

## To work on the react app
- run `make start`

## To build assets from react to the drupal 'lib folder'
- run `make build`
