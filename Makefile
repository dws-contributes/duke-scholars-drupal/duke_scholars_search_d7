SOURCE_FOLDER ?= source

LIB_FOLDER ?= lib

REPO_URL ?= git@gitlab.oit.duke.edu:dws-contributes/duke-scholars-drupal/dukescholars_search.git

clone:
	@echo @echo "cloning repo into source folder"
	git clone $(REPO_URL) -b develop source
	cd $(SOURCE_FOLDER) && npm install

start:
	@echo "running `npm run-script start`"
	cd $(SOURCE_FOLDER) && npm run-script start

build:
	@echo "running `npm run-script build` && copying compiled assets to `lib` folder"
	cd $(SOURCE_FOLDER) && npm run-script build
	rsync -r --delete $(SOURCE_FOLDER)/build/ $(LIB_FOLDER)
